/*
 * Map object
 */
var map = L.map('map');

/*
 * Map attributes
 */
x = map._container.attributes.x.value;
y = map._container.attributes.y.value;
z = map._container.attributes.z.value;

/*
 * Basemaps
 */
var basemaps = {}
var osm     = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {attribution: '© Contributeurs OpenStreetMap', maxNativeZoom: 19, maxZoom: 20}),
    gmaps   = L.tileLayer('http://mt1.google.com/vt/lyrs=m&x={x}&y={y}&z={z}' , {attribution: '© Données cartographiques Google', maxZoom: 20}),
    gsat    = L.tileLayer('https://mt1.google.com/vt/lyrs=s&x={x}&y={y}&z={z}', {attribution: '© Données cartographiques Google', maxZoom: 20});

if(map._container.attributes.base){
    base = map._container.attributes.base.value.replace(/\s+/g, '').split(",");
    base.forEach(map => {
        switch(map) {
            case 'osm':
                basemaps["OpenStreetMap"] = osm;
                break;
            case 'gmaps':
                basemaps["Google Maps"] = gmaps;
                break;
            case 'gsat':
                basemaps["Google Satellite"] = gsat;
                break;
        }
    })
    Object.values(basemaps)[0].addTo(map);
    if(Object.values(basemaps).length > 1) L.control.layers(basemaps).addTo(map);
} else osm.addTo(map);

/*
 * Map type
 */
if(map._container.attributes.type) {
    type = map._container.attributes.type.value;
    switch(type) {
        case 'position':
            L.marker([x, y]).addTo(map);
            break;
        case 'direction':
            if(map._container.attributes.angle) {
                angle = map._container.attributes.angle.value
                arrow = L.icon({
                    iconUrl: 'https://freesvg.org/img/kuba_arrow_button_set_1.png',
                    iconSize: [30,30],
                    iconAnchor: [15, 15]
                })
                L.marker([x, y], {icon: arrow, rotationAngle: angle}).addTo(map);
            }
            break;
    }
}

/*
 * Steps
 */
var latlngs = []
nodes = new URL(window.location).searchParams.getAll("node")
nodes.forEach(node => {
    var x;
    var y;
    switch(node) {
        case "a":
            x=45.776410
            y=3.089842
            break;
        case "a2":
            x=45.7764419
            y=3.0899160
            break;
        case "b":
            x=45.776437
            y=3.089958
            break;
        case "c":
            x=45.7764173
            y=3.0900020
            break;
        case "d":
            x=45.7764031
            y=3.090020
            break;
        case "e":
            x=45.7763815
            y=3.0902396
            break;
        case "f":
            x=45.776453
            y=3.090298
            break;
        case "f2":
            x=45.776353
            y=3.090357
            break;
        case "g":
            x=45.7763620
            y=3.0902597
            break;
        case "h":
            x=45.7762320
            y=3.0902597
            break;
        case "i":
            x=45.776237
            y=3.090357
            break;
        case "i2":
            x=45.776128
            y=3.090298
            break;
        case "j":
            x=45.776209
            y=3.090231
            break;
        case "k":
            x=45.776209
            y=3.090123
            break;
        case "l":
            x=45.776246
            y=3.090123
            break;
        case "m":
            x=45.776246
            y=3.090024
            break;
        case "n":
            x=45.776243
            y=3.089992
            break;
        case "o":
            x=45.776224
            y=3.089937
            break;
        case "p":
            x=45.776176
            y=3.089968
            break;
        case "p2":
            x=45.776238
            y=3.089832
            break;
        case "q":
            x=45.7762579
            y=3.0899961
            break;
        case "r":
            x=45.7763830
            y=3.0900021
            break;
    }
    latlngs.push([x,y]);
})
var polyline = L.polyline(latlngs, {color: 'red'}).addTo(map);

// Centering of the map view
map.setView([x, y],z);

// Addition of a scale
L.control.scale({imperial: false}).addTo(map);