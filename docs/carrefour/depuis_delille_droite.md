# Depuis la Place Delille sur le trottoir de droite

Vous venez de la Place Delille par le Cours Sablon sur le trottoir de droite. 

<!-- DEBUG -->
<div id=map base="gsat,osm" type="direction" x=45.7764419 y=3.0899160 z=20 angle=270></div>
<!-- DEBUG -->

[Prendre le passage piéton en direction de la Gare et du Jardin Lecoq](a2-c.md?node=a2&node=b&node=c)

[Longer le trottoir en direction de la Place Michel de l'Hospital](a2-a.md?node=a2&node=a)