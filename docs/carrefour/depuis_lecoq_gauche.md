# Depuis le Jardin Lecoq sur le trottoir de gauche

Vous venez du Jardin Lecoq par le Cours Sablon sur le trottoir de gauche.

<!-- DEBUG -->
<div id=map base="gsat,osm" type="direction" x=45.776176 y=3.089968 z=20 angle=90></div>
<!-- DEBUG -->

[Prendre le passage piéton en direction de la Place Delille et de la Gare](p-n.md?node=p&node=o&node=n)

[Longer le trottoir en direction de la Place Michel de l'Hospital](p-p2.md?node=p&node=p2)