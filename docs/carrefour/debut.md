# Description générale

Le croisement de l’Avenue Carnot et du Cours Sablon est un carrefour en croix.

L’avenue Carnot comprend quatre voies : deux voies de circulation en direction de la Gare dont la plus proche du trottoir est une voie de bus, et deux voies de bus en direction de la Place Michel-de-l’Hospital.

Le Cours Sablon comprend quatre voies : côté Gare, deux voies de circulation en direction du Boulevard Trudaine et, côté Place Michel-de-l’Hospital, deux voies de circulation en direction du Jardin Lecoq.

Une voie de service longe le trottoir depuis le Boulevard Trudaine vers la Place Michel de l'Hospital.

Deux pistes cyclables sont présentes sur les trottoirs de part et d'autre du Cours Sablon.

<!-- DEBUG -->
<div id=map base="gsat" x=45.776320 y=3.090129 z=19></div>
<!-- DEBUG -->

D'où arrivez vous ?

* Depuis la Place Delille sur le :
    * [trottoir de droite](depuis_delille_droite.md) <!-- nord-ouest -->
    * [trottoir de gauche](depuis_delille_gauche.md) <!-- nord-est -->

* Depuis la Gare sur le :
    * [trottoir de droite](depuis_gare_droite.md) <!-- nord-est -->
    * [trottoir de gauche](depuis_gare_gauche.md) <!-- sud-est -->
    
* Depuis le Jardin Lecoq sur le :
    * [trottoir de droite](depuis_lecoq_droite.md) <!-- sud-est -->
    * [trottoir de gauche](depuis_lecoq_gauche.md) <!-- sud-ouest -->

* Depuis la Place Michel de l'Hospital sur :
    * [trottoir de droite](depuis_mdh_droite.md) <!-- sud-ouest -->
    * [trottoir de gauche](depuis_mdh_gauche.md) <!-- nord-ouest -->
