# Depuis la Place Michel de l'Hospital sur le trottoir de droite

Vous venez de la Place Michel de l'Hospital par l'Avenue Carnot sur le trottoir de droite. 

<!-- DEBUG -->
<div id=map base="gsat,osm" type="direction" x=45.776238 y=3.089832 z=20 angle=180></div>
<!-- DEBUG -->

[Prendre le passage piéton en direction de la Place Delille et de la Gare](p2-n.md?node=p2&node=o&node=n)

[Longer le trottoir en direction du Jardin Lecoq](p2-p.md?node=p2&node=p)