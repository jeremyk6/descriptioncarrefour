# Depuis la Place Delille sur le trottoir de gauche

Vous venez de la Place Delille par le Cours Sablon sur le trottoir de gauche. 

<!-- DEBUG -->
<div id=map base="gsat,osm" type="direction" x=45.776453 y=3.090298 z=20 angle=270></div>
<!-- DEBUG -->

[Prendre le passage piéton en direction de la Place Michel de l'Hospital](f-d.md?node=f&node=e&node=d)

[Prendre le passage piéton en direction du Jardin Lecoq](f-h.md?node=f&node=g&node=h)

[Longer le trottoir en direction de la Gare](f-f2.md?node=f&node=f2)