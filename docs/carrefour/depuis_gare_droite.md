# Depuis la Gare sur le trottoir de droite

Vous venez de la Gare par l'Avenue Carnot sur le trottoir de droite. 

<!-- DEBUG -->
<div id=map base="gsat,osm" type="direction" x=45.776353 y=3.090357 z=20 angle=0></div>
<!-- DEBUG -->

[Prendre le passage piéton en direction de la Place Michel de l'Hospital](f2-d.md?node=f2&node=e&node=d)

[Prendre le passage piéton en direction du Jardin Lecoq](f2-h.md?node=f2&node=g&node=h)

[Longer le trottoir en direction de la Place Delille](f2-f.md?node=f2&node=f)