# Depuis la Place Michel de l'Hospital sur le trottoir de gauche

Vous venez de la Place Michel de l'Hospital par l'Avenue Carnot sur le trottoir de gauche. 

<!-- DEBUG -->
<div id=map base="gsat,osm" type="direction" x=45.776410 y=3.089842 z=20 angle=180></div>
<!-- DEBUG -->

[Prendre le passage piéton en direction de la Gare et du Jardin Lecoq](a-c.md?node=a&node=b&node=c)

[Longer le trottoir en direction de la Place Delille](a-a2.md?node=a&node=a2)