# Depuis le Jardin Lecoq sur le trottoir de droite

Vous venez du Jardin Lecoq par le Cours Sablon sur le trottoir de droite.

<!-- DEBUG -->
<div id=map base="gsat,osm" type="direction" x=45.776128 y=3.090298 z=20 angle=90></div>
<!-- DEBUG -->

[Prendre le passage piéton vers le trottoir en direction de la Place Delille](i2-g.md?node=i2&node=h&node=g)

[Prendre le passage piéton en direction de la Place Michel de l'Hospital](i2-k.md?node=i2&node=j&node=k)

[Longer le trottoir en direction de la Gare](i2-i.md?node=i2&node=i)