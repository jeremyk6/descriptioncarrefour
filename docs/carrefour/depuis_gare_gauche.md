# Depuis la Gare sur le trottoir de gauche

Vous venez de la Gare par l'Avenue Carnot sur le trottoir de gauche. 

<!-- DEBUG -->
<div id=map base="gsat,osm" type="direction" x=45.776237 y=3.090357 z=20 angle=0></div>
<!-- DEBUG -->

[Prendre le passage piéton vers le trottoir en direction de la Place Delille](i-g.md?node=i&node=h&node=g)

[Prendre le passage piéton en direction de la Place Michel de l'Hospital](i-k.md?node=i&node=j&node=k)

[Longer le trottoir en direction du Jardin Lecoq](i-i2.md?node=i&node=i2)
