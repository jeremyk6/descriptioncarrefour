Vos épaules sont parallèles au Cours Sablon. Le passage piéton se trouve à hauteur du feu rouge du Cours Sablon. Il est parallèle à l'Avenue Carnot.

<!-- DEBUG -->
???+ question "Formulation de la position du passage piéton"
    La formulation « à hauteur du feu rouge » est utilisée ici pour estimer la position du passage position. Cependant, elle ne donne pas une bonne idée de  la distance à la chaussée. Nous recherchons donc une formulation plus adaptée.
<!-- DEBUG -->
