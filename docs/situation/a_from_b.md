Vous avancez dans le sens des numéros de rue décroissants. Le prochain bâtiment porte le numéro 3.

<!-- DEBUG -->
<div id=map base="gsat,osm" type="direction" x=45.776410 y=3.089842 z=20 angle=0></div>
<!-- DEBUG -->

[Faire demi-tour](depuis_mdh_gauche.md)
