Vous avancez dans le sens des numéros de rue décroissants. Vous êtes à hauteur du numéro 13.

<!-- DEBUG -->
<div id=map base="gsat,osm" type="direction" x=45.776453 y=3.090298 z=20 angle=90></div>
<!-- DEBUG -->

[Faire demi-tour](depuis_delille_gauche.md)
