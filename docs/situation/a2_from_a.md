Vous avancez dans le sens des numéros de rue décroissants. Le prochain bâtiment porte le numéro 4.

<!-- DEBUG -->
<div id=map base="gsat,osm" type="direction" x=45.7764419 y=3.0899160 z=20 angle=90></div>
<!-- DEBUG -->

[Faire demi-tour](depuis_delille_droite.md)
