Vous avancez dans le sens des numéros de rue décroissants. Vous êtes à hauteur du numéro 16.

<!-- DEBUG -->
<div id=map base="gsat,osm" type="direction" x=45.776238 y=3.089832 z=20 angle=0></div>
<!-- DEBUG -->

[Faire demi-tour](depuis_mdh_droite.md)
