Une fois traversé le passage piéton, l'Avenue Carnot se trouve derrière vous, le Cours Sablon à votre gauche, et une bande de l'Avenue Carnot rejoint le Cours Sablon en direction du Jardin Lecoq en face.

<!-- DEBUG -->
<div id=map base="gsat,osm" type="direction" x=45.7762579 y=3.0899961 z=20 angle=270></div>
<!-- DEBUG -->

[Aller en direction de la Gare](q-l.md?node=q&node=m&node=l)

[Aller sur le trottoir en direction du Jardin Lecoq](q-o.md?node=q&node=n&node=o)

[Faire demi-tour](q-r.md?node=q&node=r)
