Une fois traversé le passage piéton, le Cours Sablon se trouve derrière vous et l'Avenue Carnot à votre droite.

<!-- DEBUG -->
<div id=map base="gsat,osm" type="direction" x=45.776209 y=3.090123 z=20 angle=0></div>
<!-- DEBUG -->

[Aller en direction de la Place Michel de l'Hospital](k-m.md?node=k&node=l&node=m)

[Faire demi-tour](k-j.md?node=k&node=j)
