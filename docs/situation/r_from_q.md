Une fois traversé le passage piéton, l'Avenue Carnot se trouve derrière vous et le Cours Sablon à votre droite.

<!-- DEBUG -->
<div id=map base="gsat,osm" type="direction" x=45.7763830 y=3.0900021 z=20 angle=90></div>
<!-- DEBUG -->

[Aller sur le trottoir en direction de la Place Michel de l'Hospital et de la Place Delille](r-b.md?node=r&node=c&node=b)

[Aller sur le trottoir en direction de la Gare](r-e.md?node=r&node=d&node=e)

[Faire demi-tour](r-q.md?node=r&node=q)
