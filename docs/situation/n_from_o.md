Une fois traversé le passage piéton, la bande de l'Avenue Carnot qui rejoint le Cours Sablon en direction du Jardin Lecoq se trouve derrière vous, l'Avenue Carnot à votre gauche et le Cours Sablon à votre droite.

<!-- DEBUG -->
<div id=map base="gsat,osm" type="direction" x=45.776243 y=3.089992 z=20 angle=135></div>
<!-- DEBUG -->

[Aller en direction de la Place Delille](n-r.md?node=n&node=q&node=r)

[Aller en direction de la Gare](n-l.md?node=n&node=m&node=l)

[Faire demi-tour](n-o.md?node=n&node=o)
