Vous avancez dans le sens des numéros de rue croissants. Le prochain bâtiment porte le numéro 5.

<!-- DEBUG -->
<div id=map base="gsat,osm" type="direction" x=45.776353 y=3.090357 z=20 angle=180></div>
<!-- DEBUG -->

[Faire demi-tour](depuis_gare_droite.md)
