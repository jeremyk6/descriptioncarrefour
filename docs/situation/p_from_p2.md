Vous avancez dans le sens des numéros de rue croissants. Le prochain bâtiment porte le numéro 8.

<!-- DEBUG -->
<div id=map base="gsat,osm" type="direction" x=45.776176 y=3.089968 z=20 angle=270></div>
<!-- DEBUG -->

[Faire demi-tour](depuis_lecoq_gauche.md)
