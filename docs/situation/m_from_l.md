Une fois traversé le passage piéton, le Cours Sablon se trouve derrière vous et l'Avenue Carnot à votre droite.

<!-- DEBUG -->
<div id=map base="gsat,osm" type="direction" x=45.776246 y=3.090024 z=20 angle=0></div>
<!-- DEBUG -->

[Aller en direction de la Place Delille](m-r.md?node=m&node=q&node=r)

[Aller sur le trottoir en direction du Jardin Lecoq et de la Place Michel de l'Hospital](m-o.md?node=m&node=n&node=o)

[Faire demi-tour](m-l.md?node=m&node=l)
