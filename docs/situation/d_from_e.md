Une fois traversé le passage piéton, le Cours Sablon se trouve derrière vous, l'Avenue Carnot à votre droite, et la voie de service en face.

<!-- DEBUG -->
<div id=map base="gsat,osm" type="direction" x=45.7764031 y=3.090020 z=20 angle=0></div>
<!-- DEBUG -->

[Aller sur le trottoir en direction de la Place Michel de l'Hospital et de la Place Delille](d-b.md?node=d&node=c&node=b)

[Aller en direction du Jardin Lecoq](d-q.md?node=d&node=r&node=q)

[Faire demi-tour](d-e.md?node=d&node=e)
