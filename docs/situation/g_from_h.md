Une fois traversé le passage piéton, l'Avenue Carnot se trouve derrière vous et le Cours Sablon à votre gauche.

<!-- DEBUG -->
<div id=map base="gsat,osm" type="direction" x=45.7763620 y=3.0902597 z=20 angle=90></div>
<!-- DEBUG -->

[Aller en direction de la Place Michel de l'Hospital](g-d.md?node=g&node=e&node=d)

[Aller en direction de la Place Delille](g-f.md?node=g&node=f)

[Aller en direction de la Gare](g-f2.md?node=g&node=f2)

[Faire demi-tour](g-h.md?node=g&node=h)
