Une fois traversé le passage piéton, le Cours Sablon se trouve derrière vous et l'Avenue Carnot à votre gauche.

<!-- DEBUG -->
<div id=map base="gsat,osm" type="direction" x=45.776246 y=3.090123 z=20 angle=180></div>
<!-- DEBUG -->

[Aller sur le trottoir en direction de la Gare](l-j.md?node=l&node=k&node=j)

[Faire demi-tour](l-m.md?node=l&node=m)
