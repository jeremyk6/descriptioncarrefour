Une fois traversé le passage piéton, la voie de service se trouve derrière vous.

<!-- DEBUG -->
<div id=map base="gsat,osm" type="direction" x=45.776437 y=3.089958 z=20 angle=45></div>
<!-- DEBUG -->

[Aller en direction de la Place Michel-de-l'Hospital](b-a.md?node=b&node=a)

[Aller en direction de la Place Delille](b-a2.md?node=b&node=a2)

[Faire demi-tour](b-c.md?node=b&node=c)
