Une fois traversé le passage piéton, le Cours Sablon se trouve derrière vous et l'Avenue Carnot à votre gauche.

<!-- DEBUG -->
<div id=map base="gsat,osm" type="direction" x=45.776209 y=3.090231 z=20 angle=180></div>
<!-- DEBUG -->

[Aller sur le trottoir en direction de la Place Delille](j-g.md?node=j&node=h&node=g)

[Aller en direction de la Gare](j-i.md?node=j&node=i)

[Aller en direction du Jardin Lecoq](j-i2.md?node=j&node=i2)

[Faire demi-tour](j-k.md?node=j&node=k)
