Une fois traversé le passage piéton, la voie de service se trouve derrière vous, le Cours Sablon en face et l'Avenue Carnot à votre droite.

<!-- DEBUG -->
<div id=map base="gsat,osm" type="direction" x=45.7764173 y=3.0900020 z=20 angle=225></div>
<!-- DEBUG -->

[Aller sur le trottoir en direction de la gare](c-e.md?node=c&node=d&node=e)

[Aller en direction du jardin Lecoq](c-q.md?node=c&node=r&node=q)

[Faire demi-tour](c-b.md?node=c&node=b)
