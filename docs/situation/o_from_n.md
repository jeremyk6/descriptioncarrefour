Une fois traversé le passage piéton, la bande de l'Avenue Carnot qui rejoint le Cours Sablon en direction du Jardin Lecoq se trouve derrière vous.

<!-- DEBUG -->
<div id=map base="gsat,osm" type="direction" x=45.776224 y=3.089937 z=20 angle=315></div>
<!-- DEBUG -->

[Aller en direction du Jardin Lecoq](o-p.md?node=o&node=p)

[Aller en direction de la Place Michel de l'Hospital](o-p2.md?node=o&node=p2)

[Faire demi-tour](o-n.md?node=o&node=n)
