Une fois traversé le passage piéton, l'Avenue Carnot se trouve derrière vous et le Cours Sablon à votre droite.

<!-- DEBUG -->
<div id=map base="gsat,osm" type="direction" x=45.7762320 y=3.0902597 z=20 angle=270></div>
<!-- DEBUG -->

[Aller en direction de la Gare](h-i.md?node=h&node=i)

[Aller en direction du Jardin Lecoq](h-i2.md?node=h&node=i2)

[Aller en direction de la Place Michel de l'Hospital](h-k.md?node=h&node=j&node=k)

[Faire demi-tour](h-g.md?node=h&node=g)
