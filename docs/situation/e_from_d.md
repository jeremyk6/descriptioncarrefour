Une fois traversé le passage piéton, le Cours Sablon se trouve derrière vous et l'Avenue Carnot à votre droite.

<!-- DEBUG -->
<div id=map base="gsat,osm" type="direction" x=45.7763815 y=3.0902396 z=20 angle=180></div>
<!-- DEBUG -->

[Aller en direction de la Place Delille](e-f.md?node=e&node=f)

[Aller en direction de la Gare](e-f2.md?node=e&node=f2)

[Aller sur le trottoir en direction du Jardin Lecoq](e-h.md?node=e&node=g&node=h)

[Faire demi-tour](e-d.md?node=e&node=d)
