Le carrefour dont vous êtes le héros vous propose d'explorer textuellement un carrefour de Clermont-Ferrand. Le carrefour étudié se situe à l'intersection entre l'Avenue Carnot et le Cours Sablon. 

<!-- DEBUG -->
<div id=map base="osm,gmaps,gsat" x=45.776320 y=3.090129 z=19></div>

???+ info "Version de développement"
    Cette version du « Carrefour dont vous êtes le héros est une version de développement. Elle ne vise pas être utilisée pour se déplacer sur le carrefour mais à présenter nos interrogations sur ces travaux. Vous pouvez nous faire part de toute remarque à l'adresse [contact.activmap@isima.fr](mailto:contact.activmap@isima.fr). D'avance, merci pour votre contribution !
<!-- DEBUG -->

[Commencer l'exploration](carrefour/debut.md)